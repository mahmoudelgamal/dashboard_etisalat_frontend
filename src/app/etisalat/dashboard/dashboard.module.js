/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('BlurAdmin.etisalat.dashboard', [])
      .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
        $stateProvider
        .state('etisalat', {
          url: '/etisalat',
          abstract: true,
          template: '<div ui-view  autoscroll="true" autoscroll-body-top></div>',
          title: 'Etisalat',
          sidebarMeta: {
            icon: 'ion-stats-bars',
            order: 0,
          },
        })
        .state('etisalat.dashboard', {
          url: '/dashboard',
          templateUrl: 'app/etisalat/dashboard/dashboard.html',
          title: 'Etisalat Dashboard',
          sidebarMeta: {
            icon: 'ion-android-home',
            order: 150,
          },
        });
  }

})();
