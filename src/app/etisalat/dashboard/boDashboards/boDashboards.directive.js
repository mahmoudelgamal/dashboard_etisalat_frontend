/**
 * @author v.lugovksy
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('BlurAdmin.etisalat.dashboard')
      .directive('boDashboards', boDashboards);

  /** @ngInject */
  function boDashboards() {
    return {
      restrict: 'E',
      controller: 'boDashboardsCtrl',
      templateUrl: 'app/etisalat/dashboard/boDashboards/boDashboards.html'
    };
  }
})();